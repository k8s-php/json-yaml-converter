<?php
/** @noinspection PhpDocSignatureInspection */
/** @noinspection PhpUnusedParameterInspection */
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\Tests;

use K8sPhp\JsonYamlConverter\Converter;
use K8sPhp\JsonYamlConverter\Exception\ConverterException;
use K8sPhp\JsonYamlConverter\File\FileReaderInterface;
use K8sPhp\JsonYamlConverter\File\FileWriterInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

final class ConverterTest extends TestCase
{
    /** @var DecoderInterface|ObjectProphecy */
    private $decoder;

    /** @var EncoderInterface|ObjectProphecy */
    private $encoder;

    /** @var FileReaderInterface|ObjectProphecy */
    private $fileReader;

    /** @var FileWriterInterface|ObjectProphecy */
    private $fileWriter;

    protected function setUp(): void
    {
        $this->decoder = $this->prophesize(DecoderInterface::class);
        $this->encoder = $this->prophesize(EncoderInterface::class);

        $this->fileReader = $this->prophesize(FileReaderInterface::class);
        $this->fileWriter = $this->prophesize(FileWriterInterface::class);
    }

    public function convertDataProvider(): array
    {
        return [
            [false, false],
            [true,  true],
            [true,  false],
        ];
    }

    /** @dataProvider convertDataProvider */
    public function testConvert(bool $srcExists, bool $targetExists): void
    {
        $decoderContext = [];
        $encoderContext = [];

        $converter = new Converter(
            $this->decoder->reveal(),
            $this->encoder->reveal(),
            $decoderContext,
            $encoderContext,
            $this->fileReader->reveal(),
            $this->fileWriter->reveal()
        );

        $srcPath = 'srcPath';
        $targetPath = 'targetPath';

        $this->fileReader->exists($srcPath)->willReturn($srcExists);
        $this->fileReader->exists($targetPath)->willReturn($targetExists);

        $srcContent = 'src-content';
        $targetContent = 'target-content';

        if (!$srcExists || $targetExists) {
            $this->expectException(ConverterException::class);
            $this->decoder->decode()->shouldNotBeCalled();
            $this->encoder->encode()->shouldNotBeCalled();
        } else {
            $this->fileReader->read($srcPath)->willReturn($srcContent);
            $decodedContent = 'src-decoded';
            $this->decoder->decode($srcContent, null, $decoderContext)->willReturn($decodedContent);
            $this->encoder->encode($decodedContent, null, $encoderContext)->willReturn($targetContent);
            $this->fileWriter->write($targetPath, $targetContent)->shouldBeCalled();
        }

        $converter->convert($srcPath, $targetPath);
    }
}
