<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\File;

use K8sPhp\JsonYamlConverter\Tests\File\FileManagerTest;
use PHPUnit\Framework\Assert;

function file_exists($path)
{
    return isset(FileManagerTest::$existingFileContent[$path]);
}

function file_get_contents($path)
{
    if (isset(FileManagerTest::$existingFileContent[$path])) {
        return FileManagerTest::$existingFileContent[$path];
    }

    return false;
}

function file_put_contents($path, $content)
{
    Assert::assertTrue(isset(FileManagerTest::$expectedFileWrites[$path]));
    Assert::assertSame(FileManagerTest::$expectedFileWrites[$path], $content);
}

namespace K8sPhp\JsonYamlConverter\Tests\File;

use InvalidArgumentException;
use K8sPhp\JsonYamlConverter\File\FileManager;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

final class FileManagerTest extends TestCase
{
    public static $existingFileContent = [];
    public static $expectedFileWrites = [];

    /** @var FileManager */
    private $fileManager;

    protected function setUp(): void
    {
        self::$existingFileContent = [
            'first.file' => 'content1',
        ];

        self::$expectedFileWrites = [
            'second.file' => 'content2',
        ];

        $this->fileManager = new FileManager();
    }

    public function testExists(): void
    {
        Assert::assertTrue($this->fileManager->exists('first.file'));
        Assert::assertFalse($this->fileManager->exists('second.file'));
    }

    public function testReadSuccess(): void
    {
        Assert::assertSame('content1', $this->fileManager->read('first.file'));
    }

    public function testReadFailure(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Could not read "not.existing.file"');

        $this->fileManager->read('not.existing.file');
    }

    public function testWrite(): void
    {
        $this->fileManager->write('second.file', 'content2');
    }
}
