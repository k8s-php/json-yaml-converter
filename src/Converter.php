<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter;

use K8sPhp\JsonYamlConverter\Exception\ConverterException;
use K8sPhp\JsonYamlConverter\File\FileReaderInterface;
use K8sPhp\JsonYamlConverter\File\FileWriterInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

final class Converter implements ConverterInterface
{
    /** @var DecoderInterface */
    private $decoder;

    /** @var EncoderInterface */
    private $encoder;

    /** @var array */
    private $decoderContext;

    /** @var array */
    private $encoderContext;

    /** @var FileReaderInterface */
    private $fileReader;

    /** @var FileWriterInterface */
    private $fileWriter;

    public function __construct(
        DecoderInterface $decoder,
        EncoderInterface $encoder,
        array $decoderContext,
        array $encoderContext,
        FileReaderInterface $fileReader,
        FileWriterInterface $fileWriter
    ) {
        $this->decoder = $decoder;
        $this->encoder = $encoder;
        $this->decoderContext = $decoderContext;
        $this->encoderContext = $encoderContext;
        $this->fileReader = $fileReader;
        $this->fileWriter = $fileWriter;
    }

    public function convert(string $sourceFilePath, string $targetFilePath): void
    {
        if (!$this->fileReader->exists($sourceFilePath)) {
            throw new ConverterException(sprintf('"%s" does not exist', $sourceFilePath));
        }

        if ($this->fileReader->exists($targetFilePath)) {
            throw new ConverterException(sprintf('"%s" already exists', $targetFilePath));
        }

        $decoded = $this->decoder->decode($this->fileReader->read($sourceFilePath), null, $this->decoderContext);
        $encoded = $this->encoder->encode($decoded, null, $this->encoderContext);

        $this->fileWriter->write($targetFilePath, $encoded);
    }
}
