<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\File;

interface FileWriterInterface
{
    public function write(string $path, string $content): void;
}
