<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\File;

interface FileReaderInterface
{
    public function exists(string $path): bool;
    public function read(string $path): string;
}
