<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\File;

use InvalidArgumentException;

final class FileManager implements FileReaderInterface, FileWriterInterface
{
    public function exists(string $path): bool
    {
        return file_exists($path);
    }

    public function read(string $path): string
    {
        $content = file_get_contents($path);

        if ($content === false) {
            throw new InvalidArgumentException(sprintf('Could not read "%s"', $path));
        }

        return $content;
    }

    public function write(string $path, string $content): void
    {
        file_put_contents($path, $content);
    }
}
