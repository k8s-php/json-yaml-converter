<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\Command;

use K8sPhp\JsonYamlConverter\ConverterInterface;
use K8sPhp\JsonYamlConverter\Exception\ConverterException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConverterCommand extends Command
{
    private const ARG_SRC_FILE = 'source_file';
    private const ARG_TARGET_FILE = 'target_file';

    /** @var ConverterInterface */
    private $converter;

    public function __construct(string $name, ConverterInterface $converter)
    {
        parent::__construct($name);
        $this->converter = $converter;
    }

    protected function configure(): void
    {
        $this
            ->addArgument(self::ARG_SRC_FILE, InputArgument::REQUIRED, 'Source file')
            ->addArgument(self::ARG_TARGET_FILE, InputArgument::REQUIRED, 'Target file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $srcFile = $input->getArgument(self::ARG_SRC_FILE);
            $targetFile = $input->getArgument(self::ARG_TARGET_FILE);

            $this->converter->convert($srcFile, $targetFile);

            $io->success(sprintf('Successfully converted "%s" to "%s"', $srcFile, $targetFile));
        } catch (ConverterException $e) {
            $io->error($e->getMessage());
        }
    }
}
