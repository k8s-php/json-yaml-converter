<?php
/** @noinspection PhpDocSignatureInspection */
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter;

use K8sPhp\JsonYamlConverter\Exception\ConverterException;

interface ConverterInterface
{
    /** @throws ConverterException */
    public function convert(string $sourceFilePath, string $targetFilePath): void;
}
