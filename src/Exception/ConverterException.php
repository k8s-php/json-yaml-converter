<?php
declare(strict_types=1);

namespace K8sPhp\JsonYamlConverter\Exception;

use Exception;

class ConverterException extends Exception
{

}
