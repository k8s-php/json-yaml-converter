Console JSON 2 YAML converter
=============================

First, much thanks to [Kristoffer Högberg's console skeleton](https://github.com/hmazter/console-skeleton) which helped getting started fast.

## Quickstart using compiled docker image

This is useful if you do not want to change the code of the converter.
The converter can be used straight away without installing anything locally except the docker image.

! ATTENTION: will be there soon !

## Build locally yourself

### Preparation

Build docker image
```bash
docker-compose build
```

Install dependencies (dev is needed to execute tests, your call)
```bash
docker-compose run console composer install [--no-dev]
```

Execute tests (remember, composer needs to install dev packages)
```bash
docker-compose run console composer test
```

### Get started

Keep in mind that the files that you want to convert need to be placed inside of this folder. Otherwise we need to add additional volume handling to the docker image.

List commands
```bash
docker-compose run console ./app
```

Convert JSON 2 YAML (file paths are relative to the project folder)
```bash
docker-compose run console ./app k8s-php:convert:json2yaml <source-file-path> <target-file-path>
```

Convert YAML 2 JSON (file paths are relative to the project folder)
```bash
docker-compose run console ./app k8s-php:convert:yaml2json <source-file-path> <target-file-path>
```

If you want to use files that are placed above this directory, you need to add the file by a dedicated volume mount
```bash
docker-compose run -v ~/file-outside.json:/tmp/src.json console ./app k8s-php:convert:yaml2json /tmp/src.json <target-file-path>
```